# Jinja pipeline renderer

Config example:

~~~
# cat .gitlab-ci/rendered-pipelines/config.yml 
default:
  ci_vars:
    SOMEVAR: SOMEVALUE
    ANOTHERVAR: 1337
  renderer_vars: {}
render_steps:
- template: flaf.yml
  ci_vars: {}
  renderer_vars:
    versions:
      - 1.0.0
      - 1.0.1
- template: flaf2.yml
  ci_vars:
    SOMEVAR: OTHERVALUE
  renderer_vars: {}
~~~

You can generally have whichever variables that you desire inside `default` and inside `render_steps`, with the exception of the `template` var in `render_steps` which is required.

But here is a recommendation of a variable structure based on the above config, and how you could utilize it in your Jinja templates:

* `default` - vars you can overwrite with vars from `render_steps`
* `ci_vars` - vars you can pass along to GitLab CI variables (Variables that are limited to the types in bash)
* `renderer_vars` - vars you can use for other Jinja things (Variables that are not limited to the types in bash)

`.gitlab-ci.yml` will run the renderer and then execute the rendered pipelines.

The above config will render into this:

~~~
# .gitlab-ci/rendered-pipelines/render.py config.yml
Adding chunk based on flaf.yml to /home/nek/rcs/krio/gitlab-jinja-pipelines/.gitlab-ci/rendered-pipelines/rendered/config.yml:
default:
  tags:
    - shared

stages:
  - flaf
  - hest

flaf-1:
  variables: 
    SOMEVAR: 'SOMEVALUE'
    ANOTHERVAR: 1337
  stage: flaf
  script:
    - echo "$SOMEVAR ko"
flaf-2:
  variables: 
    SOMEVAR: 'SOMEVALUE'
    ANOTHERVAR: 1337
  stage: flaf
  script:
    - echo "$SOMEVAR gris"
flaf-3:
  stage: flaf
  script:
    - echo "Job not implemented" && false
  allow_failure: true
flaf-4:
  stage: flaf
  script:
    - echo "Job not implemented" && false
  allow_failure: true

Adding chunk based on flaf2.yml to /home/nek/rcs/krio/gitlab-jinja-pipelines/.gitlab-ci/rendered-pipelines/rendered/config.yml:

flaf2-1.0.0-hest:
  variables: 
    SOMEVAR: 'OTHERVALUE'
    ANOTHERVAR: 1337
  stage: hest
  script:
    - echo "$SOMEVAR 1.0.0"

flaf2-1.0.1-hest:
  variables: 
    SOMEVAR: 'OTHERVALUE'
    ANOTHERVAR: 1337
  stage: hest
  script:
    - echo "$SOMEVAR 1.0.1"
~~~

Given these Jinja templates:

~~~
# cat .gitlab-ci/rendered-pipelines/templates/flaf.yml 
{% set noop = default.ci_vars.update(step.ci_vars) -%}
{% set noop = default.renderer_vars.update(step.renderer_vars) -%}
{% set template_name = step.template | replace(".yml","") -%}

default:
  tags:
    - shared

stages:
  - flaf
  - hest

{% for someloopvar in [ 'ko', 'gris', 'pony', 'giraf' ] -%}
{% if someloopvar == 'ko' or someloopvar == 'gris' -%}
{{ template_name }}-{{ loop.index }}:
  variables: {% for k,v in default.ci_vars.items() %}
    {{ k }}: {% if v is string %}'{{ v }}'{% else %}{{ v }}{% endif %}{% endfor %}
  stage: flaf
  script:
    - echo "$SOMEVAR {{ someloopvar }}"
{% else -%}
{{ template_name }}-{{ loop.index }}:
  stage: flaf
  script:
    - echo "Job not implemented" && false
  allow_failure: true
{% endif -%}
{% endfor -%}
~~~
~~~
# cat .gitlab-ci/rendered-pipelines/templates/flaf2.yml
{% set noop = default.ci_vars.update(step.ci_vars) -%}
{% set noop = default.renderer_vars.update(step.renderer_vars) -%}
{% set template_name = step.template | replace(".yml","") -%}

{% for version in default.renderer_vars.versions %}
{{ template_name }}-{{ version }}-hest:
  variables: {% for k,v in default.ci_vars.items() %}
    {{ k }}: {% if v is string %}'{{ v }}'{% else %}{{ v }}{% endif %}{% endfor %}
  stage: hest
  script:
    - echo "$SOMEVAR {{ version }}"
{% endfor %}
~~~
