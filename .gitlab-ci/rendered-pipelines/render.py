#!/usr/bin/env python3

import os
import sys
import yaml
import jinja2

script_path = os.path.dirname(os.path.abspath(sys.argv[0]))
config_file_name = sys.argv[1]

with open(f"{script_path}/{config_file_name}") as config_file:
  config = yaml.safe_load(config_file)

if os.path.exists(f"{script_path}/rendered/{config_file_name}"):
  os.remove(f"{script_path}/rendered/{config_file_name}")

jinja2_environment = jinja2.Environment(loader=jinja2.FileSystemLoader(f"{script_path}/templates/"))
for step in config['render_steps']:
  template_variables = {}
  template_variables['default'] = config['default']
  template_variables['step'] = step
  template = jinja2_environment.get_template(step['template'])

  with open(f"{script_path}/rendered/{config_file_name}", 'a', encoding="utf-8") as rendered_template:
    rendering = template.render(template_variables)

    print(f"Adding chunk based on {step['template']} to {script_path}/rendered/{config_file_name}:")
    print(rendering)

    rendered_template.write(rendering)
    rendered_template.write("\n\n")
